.. code-block:: nix

  self: super:
  {
    boost = super.boost.override {
      python = self.python3;
    };
    rr = super.callPackage ./pkgs/rr {
      stdenv = self.stdenv_32bit;
    };
  }