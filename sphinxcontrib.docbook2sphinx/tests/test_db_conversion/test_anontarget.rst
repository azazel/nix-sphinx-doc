.. _chap-quick-start:

===============================
Quick Start to Adding a Package
===============================

GNU Hello: `pkgs/applications/misc/hello/default.nix`__. Trivial package,
which specifies some *meta* attributes which is good practice.

__ https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/misc/hello/default.nix