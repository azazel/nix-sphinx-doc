.. _chap-quick-start:

===============================
Quick Start to Adding a Package
===============================

Find a good place in the Nixpkgs tree to add the Nix expression for your
package. For instance, a library package typically goes into
:file:`pkgs/development/libraries/{pkgname}`, while a web browser goes into
:file:`pkgs/{pkgname}`. See :ref:`sec-organisation` for some hints on the
tree organisation. Create a directory for your package, e.g.

.. code-block:: shell

  $ mkdir pkgs/development/libraries/libfoo

GNU Hello: `pkgs/applications/misc/hello/default.nix`__. Trivial package,
which specifies some *meta* attributes which is good practice.

__ https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/misc/hello/default.nix