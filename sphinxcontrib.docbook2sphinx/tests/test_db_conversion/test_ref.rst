.. _chap-quick-start:

===============================
Quick Start to Adding a Package
===============================

See :ref:`sec-organisation` for some hints on the tree organisation. Create
a directory for your package, e.g.