# -*- coding: utf-8 -*-
# :Project:   nix-sphinx-doc -- tests config
# :Created:   mer 24 ott 2018 22:31:51 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

from glob import glob
from os.path import dirname, exists, isabs, isdir, join, split, splitext


def load_tests_from_directory(dir, in_ext, out_ext):
    if not isabs(dir):
        dir = join(dirname(__file__), dir)
    if not isdir(dir):
        raise RuntimeError('%s does not exist or is not a directory' % dir)
    for dbfile in sorted(glob(join(dir, '*{}'.format(in_ext)))):
        db_src = open(dbfile, encoding='utf-8').read()
        cmpfile = splitext(dbfile)[0] + out_ext
        if exists(cmpfile):
            with open(cmpfile, encoding='utf-8') as f:
                rst_src = f.read()
            # The first item is to make it easier to spot the right test
            # in verbose mode
            yield split(dbfile)[1], db_src, rst_src
        else:
            raise RuntimeError('%s has no correspondent %s' % (
                dbfile, cmpfile))


def pytest_generate_tests(metafunc):
    if metafunc.cls is not None and metafunc.cls.__name__.endswith('FS'):
        ext = getattr(metafunc.cls, 'EXT', None)
        if isinstance(ext, dict):
            in_ext, out_ext = ext[metafunc.function.__name__]
        moddir = splitext(metafunc.module.__file__)[0]
        testdir =  metafunc.function.__name__
        argvalues = list(load_tests_from_directory(testdir, in_ext, out_ext))
        metafunc.parametrize(
            ('name', 'db_src', 'rst_src'), argvalues,
            ids=[v[0] if isinstance(v, tuple)
                 else v.args[0][0] for v in argvalues])
