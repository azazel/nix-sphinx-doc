# -*- coding: utf-8 -*-
# :Project:   nix-sphinx-doc -- conversion tests
# :Created:   mer 24 ott 2018 22:34:52 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

import os
import sys

from sphinxcontrib.docbook2sphinx import convert


class TestFromFS:

    EXT = {'test_db_conversion': ('.xml', '.rst'),}

    def test_db_conversion(self, name, db_src, rst_src):
        rst_dump = convert(db_src, None)
        assert rst_src == rst_dump
