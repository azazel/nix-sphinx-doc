.. -*- coding: utf-8 -*-
.. :Project:   nix-sphinx-doc --
.. :Created:   ven 26 ott 2018 12:26:05 CEST
.. :Author:    Alberto Berti <alberto@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 Alberto Berti
..

==========================================================================
 A little project to convert Nix(OS) DocBook documentation to Sphinx reSt
==========================================================================
