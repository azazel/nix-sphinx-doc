# -*- coding: utf-8 -*-
# :Project:   nix-sphinx-doc -- Python package setup
# :Created:   ven 26 ott 2018 12:11:40 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

import setuptools

with open("README.rst", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sphinxcontrib.docbook2sphinx",
    version="0.0.1",
    author="Alberto Berti",
    author_email="alberto@metapensiero.it",
    description=("A little project to convert Nix(OS) DocBook documentation "
                 "to Sphinx reSt"),
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://gitlab.com/azazel/nix-sphinx-doc",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
