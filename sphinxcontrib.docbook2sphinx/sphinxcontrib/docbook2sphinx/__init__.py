# -*- coding: utf-8 -*-
# :Project:   nix-sphinx-doc -- conversion main module
# :Created:   ven 26 ott 2018 22:55:15 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

import collections.abc
from contextlib import contextmanager
from enum import Enum
from functools import partial
import itertools
import re
import sys
import textwrap
from types import SimpleNamespace
from xml.dom import minidom

CONTEXES = {}
KEEPON = False
WRAP_WIDTH = 75
INDENT_UNIT = 2

DISPLAY = Enum('DISPLAY', ('INLINE', 'BLOCK', 'STRUCTURED'))
undefined = object()

BULLETS = ('-', '*', '+', '•', '‣', '⁃')
TITLES = ('=', '-', '~', '^', '+', '`', ':', '.', '"', '_', '*', '#')

PRINT_LINE_MARKERS = True
MARKERS_RE = re.compile(r'@.*?\|')


def flatten(elements):
    for el in elements:
        if isinstance(el, collections.abc.Iterable) and not \
           isinstance(el, str):
            yield from flatten(el)
        else:
            yield el


class Fragment:

    def __init__(self, ctx, *value, indent=0, display=None, merge=None,
                 delimiter=None):

        if isinstance(ctx, list):
            import pdb; pdb.set_trace()
        if display is None:
            display = undefined
        if merge is None:
            merge = undefined
        if len(value) == 1 and isinstance(value[0], str):
            value = value[0]
            assert value.find('\n') < 1
        else:
            value = list(flatten(value))
            if not all(map(lambda c: isinstance(c, Fragment), value)):
                raise ValueError('"value" must be all Fragment')

        if len(value) == 0:
                display = None
        self.value = value
        if display is undefined:
           self.display = None
        else:
            self.display = display
        if merge is undefined:
            self.merge = True
        else:
            self.merge = merge
        self.indent = self._calc_indent(indent)
        self.delimiter = delimiter
        self.ctx = ctx

    def __str__(self):
        return self.render(self.indent)

    def render(self, indent='', mark=None):
        if mark is None:
            mark = self.ctx.print_line_markers
        if self.display is None:
            return ''
        elif isinstance(self.value, str):
            val = self.value
            if self.display is DISPLAY.BLOCK:
                return self.indent + val
            else:
                return self.value
        else:
            res = []
            last = None
            for frag in self.value:
                fstr = str(frag)
                if frag.display is None:
                    continue
                elif last is None or not len(res):
                    res.append(fstr)
                elif len(fstr) == 0 and frag.display is DISPLAY.INLINE:
                    continue

                elif last.display is DISPLAY.BLOCK:
                    if frag.display is DISPLAY.STRUCTURED:
                        pass
                        # if res[-1][-1] != '\n' and fstr[0] != '\n' and \
                        #    len(self.value) > 1:
                        #     res.append(blankline(self.ctx, 2, 'frag.render', 1, mark))
                    else:
                        if res[-1][-1] != '\n' and fstr[0] != '\n' and \
                           len(self.value) > 1:
                            res.append(blankline(self.ctx, 1, 'frag.render1', 1,
                                                 mark))
                    res.append(fstr)
                elif last.display is DISPLAY.INLINE:
                    if frag.display is DISPLAY.INLINE:
                        if not (fstr[0] in '.,!?:;' and (len(fstr) == 1 or
                                (len(fstr) > 1 and fstr[1] == ' '))):
                            res.append(frag.delimiter)
                    else:
                        if res[-1][-1] != '\n' and fstr[0] != '\n':
                            res.append(blankline(self.ctx, 1, 'frag.render2', 1, mark))
                    res.append(fstr)
                elif last.display is DISPLAY.STRUCTURED:
                    if res[-1][-1] != '\n' and fstr[0] != '\n' and \
                       len(self.value) > 1:
                        res.append(blankline(self.ctx, 2, 'frag.render3', 1, mark))
                    res.append(fstr)
                else:
                    raise ValueError('Unknown merge')
                last = frag
            return ''.join(res)

    def __repr__(self):
        data = str(self)
        if len(data) > 15:
            data = data[:7] + '...' + data[-7:]
        return (f'<class {self.__class__.__name__!r}, {data!r},'
                f' display={self.display}>')

    def _calc_indent(self, indent):
        if isinstance(indent, int):
            return ' ' * INDENT_UNIT * indent
        else:
            if not isinstance(indent, str):
                raise ValueError('"indent" must be string or int')
            return indent

    def clone(self):
        inst = self.__class__.__new__(self.__class__)
        for attr in ('value', 'display', 'merge', 'indent',
                     'delimiter', 'ctx'):
            v = getattr(self, attr)
            if attr == 'value' and isinstance(v, list):
                v = v.copy()
            setattr(inst, attr, v)
        return inst


class Block(Fragment):

    def __init__(self, ctx,  *value, i=0, display=DISPLAY.BLOCK, merge=True,
                 aggregate=True):
        super().__init__(ctx, *value, indent=i, display=display, merge=merge,
                         delimiter='\n')
        if not isinstance(self.value, str) and aggregate:
            self.value = self._aggregate_inlines(self.value)

    def _aggregate_inlines(self, fragments):
        acc = []
        aggregated =[]
        for el in self.value:
            if isinstance(el, Inline):
                acc.append(el)
            else:
                if len(acc) > 0:
                    aggregated.append(I(self.ctx, acc))
                    acc = []
                aggregated.append(el)
        if len(acc) > 0:
            aggregated.append(I(self.ctx, acc))
        return aggregated

    def _interleave(self, frag, elements):
        return list(interleave(frag, flatten(elements)))

B = Block


class Structured(Block):
    def __init__(self, ctx,  *value, i=0, aggregate=True, interleave=True):
        super().__init__(ctx, *value, i=i, display=DISPLAY.STRUCTURED, merge=False,
                         aggregate=aggregate)
        if interleave:
            self.value = self._interleave(
                blankline(ctx, 2, 'Structured.__init__'), self.value)

S = Structured


class Directive(Structured):
    def __init__(self, ctx,  *content, i=0, name, args=None,
                 **kw):
        if args is None:
            args = []
        opts = [U(ctx, ':{}: {}'.format(k, v), i=i+1)
                for k, v in kw.items()]
        value = U(ctx,
            U(ctx, '.. {}:: {}'.format(name, ' '.join(args)), i=i),
            *opts, blankline(ctx, 2, 'Directive.__init__'),
            U(ctx, *(c(i=i+1) if callable(c) else c for c in content)))
        super().__init__(ctx, value, i=i)


class Inline(Fragment):

    def __init__(self, ctx,  *value, i=0, delimiter=' '):
        super().__init__(ctx, *value, indent=i, display=DISPLAY.INLINE, merge=True,
                         delimiter=delimiter)

I = Inline


class Itemized(Structured):

    def __init__(self, ctx,  *value, i=0, genitems='- '):
        super().__init__(ctx, *value, i=i, aggregate=True)
        self.genitems = genitems


    def render(self, indent='', mark=None):
        if mark is None:
            mark = self.ctx.print_line_markers
        res = []
        if callable(self.genitems):
            itgen = self.genitems()
        else:
            itgen = self.genitems

        for item in self.value:
            istr = item.render(mark=False)

            if mark:
                istr = ''.join(remove_makers(istr))
            # add newlines added by the _interleave in __init__
            if istr == '\n':
                continue
            if callable(itgen):
                idot = itgen()
            else:
                idot = itgen
            lines = []
            for l in istr.split('\n'):
                if not len(lines) and not len(l):
                    continue
                lines.append(l)
            bl = blankline(self.ctx, 1, 'Itemized.render', 1, mark)
            bl2 = blankline(self.ctx, 2, 'Itemized.render', 1, mark)
            indent = self.indent + ' ' * len(idot)
            to_indent = lines[1:]
            istr = textwrap.indent('\n'.join(to_indent), indent)
            res.append(self.indent + idot + lines[0].strip() + bl + istr)

        return  bl + bl2.join(res)


class Nospace(Inline):
    def __init__(self, ctx,  *value, i=0):
        super().__init__(ctx, *value, i=i, delimiter='')

N = Nospace


class Para(Structured):

    def __init__(self, ctx, *value, i=0, targets=None, wrap=True):
        super().__init__(ctx, *value, i=i)
        if wrap:
            nvalue = []
            for group in self.value:
                if not isinstance(group, Inline):
                    nvalue.append(group)
                    continue
                gstr = str(group)
                glines = wraptext(gstr, initial_indent=self.indent,
                                  subsequent_indent=self.indent)
                nvalue.append(U(ctx, *(B(ctx, line) for line in glines)))
                self.value = nvalue
        if targets is not None and len(targets.value):
            self.value = (self.value + blankline(ctx, 2, 'Para.__init__')
                          + [targets])


class Role(Inline):
    def __init__(self, ctx, name, text):
        super().__init__(ctx, ':{}:`{}`'.format(name, text))
        self.name = name
        self.text = text


class Ref(Role):
    def __init__(self, ctx, target, title=None):
        if title is None:
            super().__init__(ctx, 'ref', target)
        else:
            super().__init__(ctx, 'ref', '{} <{}>'.format(title, target))


class Section(Structured):
    def __init__(self, ctx, title, content, target=None, i=0):
        tgt = U(ctx, mix('.. _', target, ':'), i=i)
        bl2 =  blankline(ctx, 2, 'Section.__init__')
        head = U(ctx, tgt, bl2, *title, i=i)
        content = self._interleave(bl2, content)
        all = U(ctx, head, bl2, *content, i=i)
        super().__init__(ctx, all, i=i, interleave=False)


class Unmergeable(Fragment):

    def __init__(self, ctx,  *value, i=0):
        super().__init__(ctx, *value, indent=i, display=DISPLAY.BLOCK, merge=False,
                         delimiter='\n')

U = Unmergeable


def _anon_target(ctx, tgt, i):
    return U(ctx, I(ctx, '__'), I(ctx, tgt), i=i)


def _footnote_target(ctx, tgt, content, i):
    if isinstance(i, int):
        i = ' ' * ctx.indent_unit * i
    clines = str(I(ctx, *content, i=0)).splitlines()
    first = clines[0]
    bl = blankline(ctx, 1, '_footnote_target', 1)
    clines[0] = '.. [#{}] '.format(tgt) + first.strip()
    indent = bl + i
    flines = wraptext('\n'.join(clines), initial_indent=indent,
                  subsequent_indent=indent + '  ')
    return U(ctx, ''.join(flines), i=0)


@contextmanager
def _bullets(ctx):
    ctx.bullets_ix += 1
    yield BULLETS[ctx.bullets_ix]
    ctx.bullets_ix -= 1


@contextmanager
def _titlechars(ctx):
    ctx.titles_ix += 1
    yield TITLES[ctx.titles_ix]
    ctx.titles_ix -= 1


@contextmanager
def _disable_markup(ctx):
    prev = ctx.disable_markup
    ctx.disable_markup = True
    yield
    ctx.disable_markup = prev


def _code(ctx, src, i, lang=None):
    with _disable_markup(ctx):
        return Directive(ctx, *src, i=i, name='code-block',
                         args=(lang,) if lang is not None else ())


def _process_footnotes(ctx, i):
    for id, content in ctx.footnotes:
        yield _footnote_target(ctx, id, content, i)


def _process_links(ctx, i):
    for tgt in ctx.links:
        yield _anon_target(ctx, tgt, i)


def _title(ctx, text, char='=', double=True):
    text = str(text)
    tlen = len(text)
    if double:
        return U(ctx, char * tlen), U(ctx, text),  U(ctx, char * tlen)
    else:
        return U(ctx, text),  U(ctx, char * tlen)


def citerefentry(ctx, db, i):
    etitle = str(I(ctx, children(ctx, db, i, 'refentrytitle')))
    mvol = str(I(ctx, children(ctx, db, i, 'manvolnum')))
    if mvol:
        etitle += '({})'.format(mvol)
    return Role(ctx, 'manpage', etitle)


def emphasis(ctx, db, i):
    if db.parentNode.tagName == 'para':
        return I(ctx, N(ctx, '*'), N(ctx, *child(ctx, db)), N(ctx, '*'))
    else:
        return N(ctx, *child(ctx, db))


def footnote(ctx, db, i):
    id = attr(db, 'xml:id')
    ctx.footnotes.append((id, child(ctx, db, i)))
    return I(ctx, '[#{}]_'.format(id), i=i)


def filename(ctx, db, i):
    if db.parentNode.tagName == 'para':
        return Role(ctx, 'file', str(I(ctx, *child(ctx, db))))
    else:
        return child(ctx, db)


def link(ctx, db, i):
    target = attr(db, 'xlink:href')
    if target is None:
        target = attr(db, 'linkend')
    assert target is not None and target != '', (
        "Missing target value")
    ctx.links.append(target)
    return I(ctx, N(ctx, '`'), N(ctx, *child(ctx, db)), N(ctx, '`__'))


def literal(ctx, db, i):
    if db.parentNode.tagName in ('para', 'title'):
        return I(ctx, N(ctx, '``'), N(ctx, *child(ctx, db)), N(ctx, '``')),
    else:
        return I(ctx, *child(ctx, db))


def orderedlist(ctx, db, i):
    return Itemized(ctx, *child(ctx, db, i), i=i, genitems=partial(count, add='. '))


def para(ctx, db, i):
    content = list(child(ctx, db, i))
    links = tuple(_process_links(ctx, i))
    ctx.links.clear()
    return Para(ctx, *content, targets=B(ctx, links), i=i)


def programlisting(ctx, db, i):
    return _code(ctx, child_part(ctx, db), i=i, lang='nix')


def replaceable(ctx, db, i):
    if db.parentNode.tagName == 'filename':
        title = str(I(ctx, *child(ctx, db)))
        # see :file: role
        return N(ctx, '{{{}}}'.format(title))
    else:
        return emphasis(ctx, db, i)


def section(ctx, db, i):
    title = child(ctx, db, i, pred=lambda c: getattr(c, 'tagName', None) == 'title')
    content = child(ctx, db, i, pred=lambda c: getattr(c, 'tagName', None) != 'title')
    tgt = attr(db, 'xml:id')
    if len(ctx.footnotes):
        fnotes = tuple(_process_footnotes(ctx, i))
        content= (*content,
                  Directive(ctx, U(ctx, '\n'), i=i, name='rubric',
                            args=('Footnotes:',)),
                  *fnotes)
        ctx.footnotes.clear()
    return Section(ctx, title, content, tgt, i)


def screen(ctx, db, i):
    return _code(ctx, child_part(ctx, db), i=i, lang='shell')


def text_node(ctx, db, i):
    data = db.data.strip()
    data = data.replace('\t', ' ')
    if isinstance(db.parentNode, minidom.Element) and \
       db.parentNode.tagName in ('screen', 'programlisting'):
        data = [U(ctx, l, i=i) for l in data.split('\n')]
    else:
        data = [I(ctx, l.strip(), i=i) for l in data.split('\n') if len(l.strip())]
    return data


def title(ctx, db, i):
    pname = db.parentNode.tagName
    if ctx.title_is_first:
        ctx.title_is_first = False
        return U(ctx, *_title(ctx, I(ctx, *child(ctx, db, 0)), double=True), i=i)
    else:
        with _titlechars(ctx) as char:
            return U(ctx, *_title(ctx, I(ctx, *child(ctx, db, 0)), double=False, char=char))


def unorderedlist(ctx, db, i):
    with _bullets(ctx) as dot:
        return Itemized(ctx, *child(ctx, db, i), i=i, genitems=dot + ' ')


def varname(ctx, db, i):
    target = attr(db, 'linkend')
    if target:
        title = str(I(ctx, *child(ctx, db)))
        return Ref(ctx, target, title=title)
    return emphasis(ctx, db, i)


def varlistentry(ctx, db, i):
    with _disable_markup(ctx):
        name = str(N(ctx, children(ctx, db, i, 'term')))
    assert name, "Name is missing"
    body = children(ctx, db, i+1, 'listitem')
    return U(ctx, U(ctx, '**' + name +'**', i=i),
             blankline(ctx, 1, 'varlistentry'),
             U(ctx, body, i=i+1))


tags = {
    'chapter': section,
    'citerefentry': citerefentry,
    'command': lambda ctx, db, i: Role(ctx, 'command', str(I(ctx, *child(ctx, db)))),
    'emphasis': emphasis,
    'envar': lambda ctx, db, i: Role(ctx, 'envvar', str(I(ctx, *child(ctx, db)))),
    'example': section,
    'filename': filename,
    'footnote': footnote,
    'firstterm': emphasis,
    'function': emphasis, # to be replaced with something semantically
                          # more correct
    'itemizedlist': unorderedlist,
    'link': link,
    'listitem': lambda ctx, db, i: B(ctx, *child(ctx, db, i), i=i),
    'literal': literal,
    'manvolnum': lambda ctx, db, i: child(ctx, db, i),
    'markup_disabled': lambda ctx, db, i: child(ctx, db, i),
    minidom.Node.COMMENT_NODE: lambda ctx, db, i: U(ctx, I(ctx, '.. '), I(ctx, db.data), i=i),
    minidom.Node.DOCUMENT_TYPE_NODE: lambda ctx, db, i: rec(ctx, db.nextSibling, i),
    minidom.Node.TEXT_NODE: text_node,
    'note': lambda ctx, db, i: Directive(ctx, *child_part(ctx, db), i=i, name='note'),
    'option': lambda ctx, db, i: Role(ctx, 'option', str(I(ctx, *child(ctx, db)))),
    'orderedlist': orderedlist,
    'para': para,
    'programlisting': programlisting,
    'quote': lambda ctx, db, i: I(ctx, N(ctx, '“'), N(ctx, *child(ctx, db)), N(ctx, '”')),
    'refentrytitle': lambda ctx, db, i: child(ctx, db, i),
    'replaceable': replaceable,
    'screen': screen,
    'section': section,
    'term': lambda ctx, db, i: child(ctx, db, i),
    'title': title,
    'variablelist': lambda ctx, db, i: child(ctx, db, i),
    'varlistentry': varlistentry,
    'varname': varname,
    'xref': lambda ctx, db, i: Ref(ctx, attr(db, 'linkend')),
    'warning': lambda ctx, db, i: Directive(ctx, *child_part(ctx, db), i=i, name='warning'),
}


def attr(db, name):
    a = db.attributes.get(name)
    if a is not None:
        return a.firstChild.data


def blankline(ctx, n=1, m='', asstr=False, mark=None):
    if mark is None:
        mark = ctx.print_line_markers
    if mark:
        nl = '\n@{:<15}|'.format(m[:15])
    else:
        nl = '\n'
    if asstr:
        res = ''.join(nl for n in range(n))
    else:
        res = [U(ctx, nl) for n in range(n)]
        if n == 1:
            res = res[0]
    return res


def child(ctx, db, i=0, f=None, j=None, pred=None):
    if f is None:
        f = lambda e: rec(ctx, e, i)
    if pred is None:
        children = db.childNodes
    else:
        children = filter(pred, db.childNodes)
    m = map(f, children)
    if j is None:
        return m
    else:
        return interleave(j, m)


def child_part(ctx, db):
    return child(ctx, db, f=lambda e: partial(rec, ctx, e))


def children(ctx, db, i, *names):
    src = [db]
    f = lambda e: e
    src = tuple(src)
    for name in names:
        src = itertools.chain.from_iterable(
            child(ctx, c, i, f=f, pred=lambda e: getattr(e, 'tagName', None) == name)
              for c in src)
        src = tuple(src)
    src = map(lambda e: rec(ctx, e, i), src)
    return src


def count(start=1, *args, **kw):
    i = start
    def c(add=''):
        nonlocal i
        res = str(i)
        i += 1
        return res + add
    return partial(c, *args, **kw)


def getctx(db):
    if db.nodeType == db.DOCUMENT_NODE:
        key = db
    else:
        key = db.ownerDocument
    if key in CONTEXES:
        ctx = CONTEXES[key]
    else:
        ctx = SimpleNamespace(
            bullets_ix=-1,
            disable_markup=False,
            footnotes=[],
            indent_unit=INDENT_UNIT,
            keepon=KEEPON,
            links=[],
            print_line_markers=PRINT_LINE_MARKERS,
            title_is_first=True,
            titles_ix=-1,
            wrap_width=WRAP_WIDTH
        )
        CONTEXES[key] = ctx
    return ctx


def ind(text, i=0):
    if isinstance(i, int):
        i = '  ' * i
    return textwrap.indent(text, i)


def jmap(s, f, *l):
    """Shorthand for the join+map operation"""
    els = []
    for el in map(f, *l):
        if isinstance(el, list):
            el = s.join(el)
        els.append(el)
    return s.join(els)


def interleave(frag, elements):
    it = iter(elements)
    try:
        nx = next(it)
        while True:
            yield nx
            nx = next(it)
            if isinstance(frag, collections.abc.Sequence):
                for f in frag:
                    yield f.clone()
            else:
                yield frag.clone()
    except StopIteration:
        pass


def mix(*x):
    """Join everything together if none of them are empty"""
    return ''.join(x) if all(x) else ''


def parse(ctx, db):
    """Converts an docbook tree to sphinx's reST"""
    return str(rec(ctx, db, 0))


def rec(ctx, db, i=0):
    if db.nodeType == db.DOCUMENT_NODE:
        return rec(ctx, db.firstChild)
    elif db.nodeType == db.ELEMENT_NODE:
        if ctx.disable_markup:
            selector = 'markup_disabled'
        else:
            selector = db.tagName
    else:
        selector = db.nodeType
    if selector not in tags and KEEPON is True:
        return I('')
    return tags[selector](ctx, db, i)


def remove_makers(*text):
    for t in text:
        yield MARKERS_RE.sub('', t)


def split(text):
    if isinstance(text, str):
        return [l for l in text.split('\n') if len(l) > 0]
    else:
        return text


def tabs(i):
    return '\n' + '  '*i


def wraptext(text, initial_indent, subsequent_indent=None, width=WRAP_WIDTH):
    if subsequent_indent is None:
        subsequent_indent = initial_indent
    return textwrap.wrap(text, width=width,
                         break_long_words=False,
                         break_on_hyphens=False,
                         initial_indent=initial_indent,
                         subsequent_indent=subsequent_indent)


def convert(fin, fout, debug=False):
    if hasattr(fin, 'read'):
        doc = fin.read()
    else:
        assert isinstance(fin, str)
        doc = fin

    dbxml = minidom.parseString(doc)
    ctx = getctx(dbxml)
    ctx.print_line_markers = debug
    try:
        res = parse(ctx, dbxml)
    finally:
        del CONTEXES[dbxml]
    if hasattr(fout, 'write'):
        fout.write(res)
    else:
        return res
