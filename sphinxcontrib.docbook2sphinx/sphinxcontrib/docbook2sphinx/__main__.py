# -*- coding: utf-8 -*-
# :Project:   nix-sphinx-doc -- main module
# :Created:   ven 26 ott 2018 12:40:58 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

import argparse
import sys

from . import convert


parser = argparse.ArgumentParser(description='DocBook to Sphinx '
                                 'reStructuredText converter')
parser.add_argument('docbook_file', nargs='?', default=None,
                    help='a DocBook file')
parser.add_argument('-d', '--debug', action='store_true',
                    help='activate debugging')
parser.add_argument('--pdb', action='store_true',
                    help="Enter post-mortem debug when an error occurs")

args = parser.parse_args()

if args.docbook_file is None:
    fin = sys.stdin
else:
    fin = open(args.docbook_file, 'r', encoding="utf-8")

try:
    convert(fin, sys.stdout, debug=args.debug)
except Exception as e:
    if args.pdb:
        import pdb
        pdb.post_mortem(e.__traceback__)
    else:
        raise
