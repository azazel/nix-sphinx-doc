# -*- coding: utf-8 -*-
# :Project:   nix-sphinx-doc -- nix-shell config file
# :Created:   ven 26 ott 2018 22:53:58 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "doc-sphinx-shell";

  buildInputs = [
    python3Full
    python3Packages.virtualenv
    python3Packages.pip
    python3Packages.sphinx
  ];
  shellHook = ''
  # set SOURCE_DATE_EPOCH so that we can use python wheels
  SOURCE_DATE_EPOCH=$(date +%s)
  if ! [ -a env ]; then
      python3 -m venv --system-site-packages env
      export PATH=$PWD/env/bin:$PATH
      pip install -r requirements.txt
  fi
  export PATH=$PWD/env/bin:$PATH
  '';

}
